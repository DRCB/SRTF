from nodo import Nodo

class Lista(object):
    def __init__(self):
        self.__primero = None
        self.__ultimo = None
    
    def vacio(self):
        if self.__primero == None:
            return True
    
    def insertarNodoAlInicio(self, elemento):
        nodo = Nodo(elemento)
        if self.vacio() == True:
            self.__primero = self.__ultimo = nodo
        else:
            nodo.pNodo = self.__primero
            self.__primero = nodo
    
    def insertarNodoAlFinal(self,elemento):
        nodo = Nodo(elemento)
        if self.vacio() == True:
            self.__primero = self.__ultimo = nodo
        else:
            self.__ultimo.pNodo = nodo
            self.__ultimo = nodo
    
    def eliminarPrimerNodo(self):
        if self.vacio() == True:
            print("Lista vacia. Imposible eliminar")
        elif self.__primero == self.__ultimo:
            self.__primero = None
            self.__ultimo = None
            #print ("Elemento eliminado. La lista esta vacia")
        else:
            temp = self.__primero
            self.__primero = self.__primero.pNodo
            temp = None
            #print("Primer Elemento eliminado")
        
    def eliminarUltimoNodo(self):
        if self.vacio() == True:
            print("Lista vacia. Imposible eliminar")
        elif self.__primero == self.__ultimo:
            self.__primero = None
            self.__ultimo = None
            #print("Elemento eliminado. La lista esta vacia")
        else:
            validar = True
            temp = self.__primero
            while(validar):
                if temp.pNodo == self.__ultimo:
                    temp2 = self.__ultimo
                    self.__ultimo = temp
                    temp2 = None
                    validar = False
                    #print("Ultimo Elemento eliminado")
                else:
                    temp = temp.pNodo
    
    def getPrimerNodo(self):
        if self.vacio() == True:
            return ("Lista vacia")
        else:
            return self.__primero
    
    def getUltimoNodo(self):
        if self.vacio() == True:
            return ("Lista vacia")
        else:
            return self.__ultimo
    
    def verListaPrimeroAUltimo(self):
        if self.vacio() == True:
            print("Lista vacia")
        else:
            Validar = True
            temp = self.__primero
            while(Validar):
                print (temp.getElemento())
                if temp == self.__ultimo:
                    Validar = False
                else:
                    temp = temp.pNodo

# lista = Lista()
# lista.insertarNodoAlFinal(1)
# lista.insertarNodoAlFinal("loca")
# lista.insertarNodoAlFinal(True)
# lista.verListaPrimeroAUltimo()
# lista.insertarNodoAlInicio("nuevo inicio")
# lista.verListaPrimeroAUltimo()
# lista.insertarNodoAlInicio("pedro")
# lista.verListaPrimeroAUltimo()
# lista.eliminarUltimoNodo()
# lista.verListaPrimeroAUltimo()
# lista.eliminarPrimerNodo()
# lista.verListaPrimeroAUltimo()
# print(lista.getPrimerNodo().getElemento())
# print(lista.getUltimoNodo().getElemento())
# lista.eliminarUltimoNodo()
# lista.eliminarUltimoNodo()
# lista.eliminarUltimoNodo()
# lista.eliminarUltimoNodo()
# lista.eliminarPrimerNodo()