from srtf import  Procesador, Proceso, Recurso
import json
import Queue
from websocket_server import WebsocketServer

def nuevo_cliente(client, server):
	global procesador1
	global procesador2
	global procesador3
	global colaSincronizacionProcesador1
	global colaSincronizacionProcesador2
	global colaSincronizacionProcesador3
	global recursos
	recursos = []
	colaSincronizacionProcesador1 = Queue.Queue()
	colaSincronizacionProcesador2 = Queue.Queue()
	colaSincronizacionProcesador3 = Queue.Queue()
	print("Un nuevo cliente se ha conectado y se le asigno el id %d" % client['id'])
	procesador1 = Procesador("procesador1",colaSincronizacionProcesador1)
	procesador2 = Procesador("procesador2",colaSincronizacionProcesador2)
	procesador3 = Procesador("procesador3",colaSincronizacionProcesador3)
	procesador1.setConexion(server,client)
	procesador2.setConexion(server,client)
	procesador3.setConexion(server,client)
	procesador1.start()
	procesador2.start()
	procesador3.start()
	mensaje = json.JSONEncoder().encode('Bienvenido cliente '+ str(client['id']))
	server.send_message(client,mensaje)



def cliente_seFue(client, server):
	print("Cliente(%d) se ha desconectado" % client['id'])


def mensaje_recibido(client, server, message):
	print("Cliente(%d) dijo: %s" % (client['id'], message))
	global procesador1
	global procesador2
	global procesador3
	global recursos
	peticion = json.JSONDecoder().decode(message)
	if peticion["tipo"] == "proceso":
		try:
			recurso = next(recurso for recurso in recursos if recurso.nombre == peticion["recurso"])
		except StopIteration:
			recurso = None
		if recurso == None:
			recurso = Recurso(peticion["recurso"])
			recursos.append(recurso)
		proceso = Proceso(peticion["nombre"],peticion["tiempo"],recurso)
		if peticion["procesador"] == "Jugador 1":
			procesador1.agregarProceso(proceso)
		elif peticion["procesador"] == "Jugador 2":
			procesador2.agregarProceso(proceso)
		elif peticion["procesador"] == "Jugador 3":
			procesador3.agregarProceso(proceso)
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Proceso Creado"})
	elif peticion["tipo"] == "recurso":
		try:
			recurso = next(recurso for recurso in recursos if recurso.nombre == peticion["nombre"])
		except StopIteration:
			recurso = None
		if recurso == None:
			recurso = Recurso(peticion["nombre"])
			recursos.append(recurso)
			mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Recurso Creado"})
		else:
			mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"EL recurso ya existe"})
	elif peticion["tipo"] == "pausar":
		procesador1.pausar()
		procesador2.pausar()
		procesador3.pausar()
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Los procesadores se han pausado"})
	elif peticion["tipo"] == "resumir":
		procesador1.resumir()
		procesador2.resumir()
		procesador3.resumir()
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Los procesadores se han activado"})
	elif peticion["tipo"] == "tiempo":
		if peticion["procesador"] == "Jugador 1":
			tiempo = procesador1.getTiempo()
		elif peticion["procesador"] == "Jugador 2":
			tiempo = procesador2.getTiempo()
		elif peticion["procesador"] == "Jugador 3":
			tiempo = procesador3.getTiempo()
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":tiempo})
	elif peticion["tipo"] == "escala":
		escala = int(peticion["escala"])
		procesador1.setEscala(escala)
		procesador2.setEscala(escala)
		procesador3.setEscala(escala)
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Se ha cambiado la escala"})
	elif peticion["tipo"] == 'expulsion':
		if peticion["procesador"] == '1':
			procesador1.setDetener(True)
		elif peticion["procesador"] == '2':
			procesador2.setDetener(True)
		elif peticion["procesador"] == '3':
			procesador3.setDetener(True)
		mensaje = json.JSONEncoder().encode({"callback_id":peticion["callback_id"],"respuesta":"Se ha detenido la ejecucion del proceso"}) 	

	server.send_message(client,mensaje)



procesador1 = None
procesador2 = None
procesador3 = None
recursos = []
colaSincronizacionProcesador1 = None
colaSincronizacionProcesador2 = None
colaSincronizacionProcesador3 = None


PORT=9001
server = WebsocketServer(PORT)
server.set_fn_new_client(nuevo_cliente)
server.set_fn_client_left(cliente_seFue)
server.set_fn_message_received(mensaje_recibido)
server.run_forever()
 