from lista import Lista

class Cola(object):
    def __init__(self):
        self.__lista = Lista()
        self.tamanio = 0
    
    def push(self,elemento):
        self.__lista.insertarNodoAlFinal(elemento)
        self.tamanio += 1
    
    def pop(self):
        nodo = self.__lista.getPrimerNodo()
        self.__lista.eliminarPrimerNodo()
        self.tamanio -= 1
        return nodo.getElemento() # Se cambio esta linea para que sea mas usable en Round Robin, antes la linea era 'return nodo'

    def vacia(self):
        return self.__lista.vacio()

    def getPrimerElemento(self):
        nodo = self.__lista.getPrimerNodo()
        return nodo.getElemento()
    
    def recorrerCola(self):
        self.__lista.verListaPrimeroAUltimo()

# cola = Cola()
# cola.push("andres")
# cola.push("camilo")
# cola.push("ana")
# cola.push("mario")
# cola.push("luisa")
# cola.recorrerCola()
# nombre=cola.pop()
# print("el pop devolvio", nombre.getElemento())
# cola.recorrerCola()
# cola.pop()
# cola.pop()
# cola.pop()
# cola.recorrerCola()
# cola.push(4)
# cola.recorrerCola()
# nombre=cola.pop()
# print("el pop devolvio", nombre.getElemento())
