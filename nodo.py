class Nodo (object):
    def __init__(self, elemento):
        self.__elemento = elemento
        self.pNodo = None

    def getElemento(self):
        return self.__elemento

# nodo = Nodo(1)
# nodo2 = Nodo("yeah")
# nodo3 = Nodo(True)
# print(nodo.getElemento())
# print(nodo2.getElemento())
# print(nodo3.getElemento())